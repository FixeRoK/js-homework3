let userNumber;

while (true) {
    userNumber = prompt('Enter number:');

    if (userNumber === null) {
        console.log('Cansel');
        break;
    }

    if (Number.isInteger(Number(userNumber))) {
        userNumber = Number(userNumber);
        break;
    }

    console.log('Enter whole number');
}

if (Number.isInteger(userNumber)) {
    if (userNumber < 5) {
        console.log('No numbers');
    } else {
        let result = '';

        for (let i = 0; i <= userNumber; i++) {
            if (i % 5 === 0) {
                result += i + ' ';
            }
        }

        if (result === '') {
            console.log('No numbers');
        } else {
            console.log(result);
        }
    }
}





function isPrime(num) {
    if (num < 2) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
}

let m, n;

do {
    m = parseInt(prompt('Enter lower number:'));
    n = parseInt(prompt('Enter larger number:'));
} while (!Number.isInteger(m) || !Number.isInteger(n) || m >= n);

console.log(`Прості числа між ${m} та ${n}:`);

for (let i = m; i <= n; i++) {
    if (isPrime(i)) {
        console.log(i);
    }
}
